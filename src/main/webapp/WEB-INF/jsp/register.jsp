
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>r</title>
</head>
<body>
<h1>Registration</h1>
<form:form id="user/register" action="/users" method="post" modelAttribute="user">
    <form:errors  path = ""  element = "div" />
    Login:<br/>
    <form:input path="login" value="" />
    <form:errors path="login"  /><br>
    Password:<br/>
    <form:input path="password" />
    <form:errors path="password"  /><br>
    Email:<br/>
    <form:input path="email"  />
    <form:errors path="email"  /><br>
    <form:button name="Register">Register</form:button>
</form:form>

</body>
</html>
