package org.timoshuk.service;

import org.timoshuk.exeption.EmailExistsException;
import org.timoshuk.exeption.LoginExistsException;
import org.timoshuk.model.User;

public interface UserService {

    void create(User user) throws EmailExistsException, LoginExistsException;
    User findById(Long id);
    void update(User user);
    User findByEmail(String emsil);
    public User findByLogin(String login);
}
