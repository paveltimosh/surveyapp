package org.timoshuk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.timoshuk.DAO.impl.UserDAOImpl;
import org.timoshuk.exeption.EmailExistsException;
import org.timoshuk.exeption.LoginExistsException;
import org.timoshuk.model.User;
import org.timoshuk.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAOImpl userDAO;

    @Transactional
    @Override
    public void create(User user) throws EmailExistsException, LoginExistsException {
        if(findByEmail(user.getEmail()) != null){
            throw new EmailExistsException("User with this email is also exists!");
        }
        if(findByLogin(user.getLogin()) != null){
            throw new LoginExistsException("User with this login is also exists!");
        }
        userDAO.create(user);
    }

    @Transactional
    @Override
    public User findByLogin(String login) {
        return userDAO.findByLogin(login);
    }

    @Transactional
    @Override
    public User findById(Long id) {
        return userDAO.findById(id);
    }

    @Transactional
    @Override
    public void update(User user){
        userDAO.update(user);
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }


}
