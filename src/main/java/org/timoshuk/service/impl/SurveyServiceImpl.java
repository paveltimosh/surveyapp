package org.timoshuk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.timoshuk.DAO.impl.SurveyDAOImpl;
import org.timoshuk.DAO.impl.UserDAOImpl;
import org.timoshuk.exeption.CloseSurveyAccsessException;
import org.timoshuk.exeption.NumberOfAnswersException;
import org.timoshuk.exeption.SurveyIsClosedException;
import org.timoshuk.exeption.SurveyNotContainAnswerException;
import org.timoshuk.model.Answer;
import org.timoshuk.model.Survey;
import org.timoshuk.model.User;
import org.timoshuk.service.SurveyService;
import org.timoshuk.util.LinkGenerator;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SurveyServiceImpl implements SurveyService {

    @Autowired
    @Qualifier(value = "surveyDAOImpl")
    private SurveyDAOImpl surveyDAO;

    @Autowired
    private UserDAOImpl userDAO;

    @Transactional
    @Override
    public Survey findById(Long id) {
        return surveyDAO.findById(id);
    }

    @Transactional
    @Override
    public void create(Survey survey) {
        createFullEntity(survey);
        surveyDAO.create(survey);
    }

    @Transactional
    @Override
    public void update(Survey survey) {
        surveyDAO.update(survey);
    }

    @Transactional
    @Override
    public List<Survey> findAll() {
        return surveyDAO.findAll();
    }

    @Override
    public void acceptAnswer(Survey survey, Long idOfSelesctedAnswer) throws SurveyIsClosedException {
        boolean surveyContainsThisAnswer = false;
        if (!(survey.isActive())){
            throw new SurveyIsClosedException("You cannot vote because the survey is already closed!");
        }
        for (Answer answer : survey.getAnswers()){
            if (answer.getId() == idOfSelesctedAnswer){
                int numberOfViuters = answer.getNumberOfVoiters();
                numberOfViuters++;
                answer.setNumberOfVoiters(numberOfViuters);
                surveyContainsThisAnswer = true;
            }
        }
        if (!surveyContainsThisAnswer){
            throw new SurveyNotContainAnswerException("This Survey doesn't contain this answer!");
        }
    }

    @Override
    public Survey findByLink(String link) {
        return surveyDAO.findByLink(link);
    }

    @Override
    public void closeSurvey(Survey survey, String login) {
        User user = userDAO.findByLogin(login);
        Long userId = user.getId();
        if (!userId.equals(survey.getCreatedUserId())){
            throw new CloseSurveyAccsessException("Нou are not a survey owner, you can not close it");
        }else {
            survey.setActive(false);
        }
    }

    private void createFullEntity(Survey survey){
        survey.setActive(true);
        survey.setDateTimeOfCreation(LocalDateTime.now());
        survey.setGeneratedLink(LinkGenerator.getGeneratedLink());
        if(survey.getAnswers().size() <= 1 || survey.getAnswers().size() >10){
            throw new NumberOfAnswersException("Number of answers cannot be more then 10 and less then 1 !");
        }
        for (Answer answer : survey.getAnswers()) {
            answer.setNumberOfVoiters(0);
        }
    }
}
