package org.timoshuk.service;

import org.timoshuk.exeption.SurveyIsClosedException;
import org.timoshuk.model.Survey;

import java.security.Principal;
import java.util.List;

public interface SurveyService {

    Survey findById(Long id);
    void create(Survey survey);
    void update(Survey survey);
    List<Survey> findAll();
    void acceptAnswer(Survey survey, Long id) throws SurveyIsClosedException;
    Survey findByLink(String link);
    void closeSurvey(Survey survey, String login);

}
