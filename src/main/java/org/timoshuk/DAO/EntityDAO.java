package org.timoshuk.DAO;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface EntityDAO <T, K>  {

    T findById(K id);
    List<T> findAll();
    void create(final T entity);
    void update(final T entity);
    void delete(final T entity);
    void deleteById(K id);

}
