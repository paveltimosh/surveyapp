package org.timoshuk.DAO.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.timoshuk.DAO.AbstractEntityDAO;
import org.timoshuk.model.User;

@Repository
public class UserDAOImpl extends AbstractEntityDAO <User, Long> {

    public UserDAOImpl() {
        setType(User.class);
    }

    public User findByLogin (String login){
        User user = null;
        String sql = "FROM User where login " + " =:paramValue";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("paramValue", login);
        user = (User) query.uniqueResult();
        return user;
    }

    public User findByEmail (String email){
        User user = null;
        String sql = "FROM User where email " + " =:paramValue";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("paramValue", email);
        user = (User) query.uniqueResult();
        return user;
    }
}
