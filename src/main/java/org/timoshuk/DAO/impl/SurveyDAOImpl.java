package org.timoshuk.DAO.impl;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.timoshuk.DAO.AbstractEntityDAO;
import org.timoshuk.model.Survey;
import org.timoshuk.model.User;

@Repository
public class SurveyDAOImpl extends AbstractEntityDAO <Survey, Long>{

    public SurveyDAOImpl() {
        setType(Survey.class);
    }

    public Survey findByLink (String link){
        Survey survey = null;
        String sql = "FROM Survey where generated_link" + " =:paramValue";
        Query query = getCurrentSession().createQuery(sql);
        query.setParameter("paramValue", link);
        survey = (Survey) query.uniqueResult();
        return survey;
    }
}
