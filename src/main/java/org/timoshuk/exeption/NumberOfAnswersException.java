package org.timoshuk.exeption;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class NumberOfAnswersException extends RuntimeException{

    public NumberOfAnswersException() {
    }

    public NumberOfAnswersException(String message) {
        super(message);
    }
}
