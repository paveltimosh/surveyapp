package org.timoshuk.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class SurveyNotContainAnswerException extends RuntimeException {

    public SurveyNotContainAnswerException() {
    }

    public SurveyNotContainAnswerException(String message) {
        super(message);
    }
}
