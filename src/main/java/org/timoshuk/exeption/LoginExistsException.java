package org.timoshuk.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class LoginExistsException extends RuntimeException {

    public LoginExistsException() {
    }

    public LoginExistsException(String message) {
        super(message);
    }
}
