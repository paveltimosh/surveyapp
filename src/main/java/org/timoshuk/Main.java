package org.timoshuk;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.timoshuk.service.UserService;

public class Main {

    @Autowired
    private UserService userService;


    public static void main(String[] args) {
        int length = 20;
        boolean useLetters = true;
        boolean useNumbers = true;
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        System.out.println(generatedString);
    }


}
