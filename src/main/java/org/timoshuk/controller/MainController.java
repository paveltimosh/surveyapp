package org.timoshuk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.timoshuk.service.SurveyService;
import org.timoshuk.service.UserService;

@RestController
@RequestMapping("/")
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private SurveyService surveyService;

    @GetMapping
    @ResponseBody
    public ModelAndView getWelcome(){
        ModelAndView modelAndView= new ModelAndView("/WEB-INF/index.jsp");
        return modelAndView;
    }
}
