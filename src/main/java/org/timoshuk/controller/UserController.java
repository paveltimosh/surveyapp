package org.timoshuk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.timoshuk.exeption.EmailExistsException;
import org.timoshuk.exeption.LoginExistsException;
import org.timoshuk.model.User;
import org.timoshuk.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public User registerNewUser(@Valid @RequestBody User user) throws EmailExistsException, LoginExistsException {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.create(user);
        return user;
    }
}
