package org.timoshuk.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.timoshuk.exeption.SurveyIsClosedException;
import org.timoshuk.model.Survey;
import org.timoshuk.model.User;
import org.timoshuk.service.SurveyService;
import org.timoshuk.service.UserService;
import java.security.Principal;

@Controller
@RequestMapping("/surveys")
public class SurveyController {

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Survey createNewSurvey(@RequestBody Survey survey,
                                  Principal principal){
        User user = userService.findByLogin(principal.getName());
        survey.setCreatedUserId(user.getId());
        surveyService.create(survey);
        return surveyService.findById(survey.getId());
    }

    @RequestMapping(value = "/{link}/answers/{answer_id}", method = RequestMethod.PUT)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Survey choseAnswer(@PathVariable("link") String link,
                           @PathVariable("answer_id") Long idOfSelectedAnswer) throws SurveyIsClosedException {
        Survey survey = surveyService.findByLink(link);
        surveyService.acceptAnswer(survey, idOfSelectedAnswer);
        surveyService.update(survey);
        return survey;
    }

    @ResponseBody
    @RequestMapping(value = "/{link}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Survey getSurveyByLink (@PathVariable("link") String link){
        return surveyService.findByLink(link);
    }

    @ResponseBody
    @RequestMapping(value = "/{link}/close", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Survey closeSurvey(@PathVariable("link") String link,
                              Principal principal){
        Survey survey = surveyService.findByLink(link);
        surveyService.closeSurvey(survey, principal.getName());
        surveyService.update(survey);
        return survey;
    }

    /*@RequestMapping(value = "/show_all", method = RequestMethod.GET)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @JsonView( SurveyView.TopicAnswerDateActive.class)
    public List<Survey> showAllSurveys(){
        return surveyService.findAll();
    }*/

}
