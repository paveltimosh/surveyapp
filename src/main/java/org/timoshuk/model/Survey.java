package org.timoshuk.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.Id;
import org.timoshuk.model.views.AnswerView;
import org.timoshuk.model.views.SurveyView;
import org.timoshuk.util.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "survey", schema = "public")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Survey implements Serializable {

    private static final long serialVersionUID = -2960839352391609975L;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "survey_seq")
    @SequenceGenerator(name = "survey_seq", sequenceName = "survey_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "id_of_user")
    private Long createdUserId;

    @Column(name = "topic")
    @JsonView(SurveyView.TopicAnswerDateActive.class)
    private String topic;

    @Column(name = "date_time_creating", updatable = false)
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonView(SurveyView.TopicAnswerDateActive.class)
    private LocalDateTime dateTimeOfCreation;

    @JsonView(SurveyView.TopicAnswerDateActive.class)
    @Column(name = "is_active")
    private boolean isActive;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_survey", referencedColumnName = "id")
    @JsonView({AnswerView.DescripNumOfVoiters.class, SurveyView.TopicAnswerDateActive.class})
    private List<Answer> answers;

    @Column(name = "generated_link")
    private String generatedLink;

}
