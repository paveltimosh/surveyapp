package org.timoshuk.model;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.timoshuk.model.views.AnswerView;
import org.timoshuk.model.views.SurveyView;

import javax.persistence.Id;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "answer", schema = "public")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Answer implements Serializable {

    private static final long serialVersionUID = 3802026611247302934L;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "answer_seq")
    @SequenceGenerator(name = "answer_seq", sequenceName = "answer_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_survey")
    private Long idOfSurvey;


    @JsonView({AnswerView.DescripNumOfVoiters.class, SurveyView.TopicAnswerDateActive.class})
    @Column(name = "description")
    private String descriptionOfAnswer;

    @JsonView({AnswerView.DescripNumOfVoiters.class, SurveyView.TopicAnswerDateActive.class})
    @Column(name = "number_of_voiters")
    private Integer numberOfVoiters;

    public Long getId() {
        return id;
    }

    public Long getIdOfSurvey() {
        return idOfSurvey;
    }

    public void setIdOfSurvey(Long idOfSurvey) {
        this.idOfSurvey = idOfSurvey;
    }

    public String getDescriptionOfAnswer() {
        return descriptionOfAnswer;
    }

    public void setDescriptionOfAnswer(String descriptionOfAnswer) {
        this.descriptionOfAnswer = descriptionOfAnswer;
    }

    public Integer getNumberOfVoiters() {
        return numberOfVoiters;
    }

    public void setNumberOfVoiters(Integer numberOfVoiters) {
        this.numberOfVoiters = numberOfVoiters;
    }
}
