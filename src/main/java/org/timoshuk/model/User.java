package org.timoshuk.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Builder
@Data
@Table(name = "users", schema = "public")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = -3961240478030776048L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "users_id_seq", allocationSize = 1)
    private Long id;

    @Column
    @Email(message = "Email incorrect! It must be like: email@email.com!")
    @NotEmpty(message = "This field cannot be empty!")
    private String email;

    @Column
    @NotEmpty(message = "This field cannot be empty!")
    @Size(min = 3, max = 25, message = "Login cannot be longer than 30 characters or less than 3 characters!")
    private String login;

    @Column
    @NotEmpty(message = "This field cannot be empty!")
    private String password;

}
