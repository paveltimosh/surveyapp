package org.timoshuk.util;

import org.apache.commons.lang.RandomStringUtils;

public class LinkGenerator {

    public static String getGeneratedLink() {
        int length = 20;
        String generatedString = RandomStringUtils.random(length, true, true);
        return generatedString;
    }
}
