SurveyApp

##**Взаимодействие с веб-службой**

Доступ к ресурсам магазина можно получить с использованием 
любого HTTP-клиента, поддерживающего Basic-аутентификацию.

##**Регистрация пользователя**

Регистрация нового пользователя осуществляется отправкой запроса POST /users:
header:   {Content-Type: application/json, charset=UTF-8}
body:{

     	"login" : "Petr",
     	"email" : "petr@petr.com",
     	"password" : "password"
     }
Если данные не прошли валидацию, или пользователь с таким email или login уже существует,то сервер выдаст ошибку
406 Not Acceptable с описанием ошибки.
 
##**Создание нового голосования**

Создание нового голосования осуществляется отправкой запроса POST /surveys:
header:   Content-Type: application/json; charset=UTF-8
body:{

        "topic" : "To be or not to be?",
        "answers" : [
            {
                "descriptionOfAnswer" : "To be"
            },
            {
                "descriptionOfAnswer" : "Not to be"
            }
            ]
    } 
    
Сервер вернет опрос, который будет содержать сгенерированную ссылку для доступа:
{

    "id": 97,
    "createdUserId": 26,
    "topic": "To be or not to be?",
    "dateTimeOfCreation": "2018-12-12 13:59:04",
    "answers": [
        {
            "id": 69,
            "idOfSurvey": 97,
            "descriptionOfAnswer": "To be",
            "numberOfVoiters": 0
        },
        {
            "id": 70,
            "idOfSurvey": 97,
            "descriptionOfAnswer": "To be",
            "numberOfVoiters": 0
        }
    ],
    "generatedLink": "DWtbYpz5J5O3bqSJG3Rf",
    "active": true
}
Если было создано меньше чем 1 и больше чем 10 ответов в опросе, то сервер вернет ошибку.
Если пользователь не авторизован, то сервер сообщит об ошибке доступа.

##**Получение данных опроса**

Получить данные опроса можно отправкой запроса GET /surveys/{generatedLink}.
Если пользователь не авторизован, то сервер сообщит об ошибке доступа.


##**Голосование в опросе**

Для отправки голоса в опросе необходимо отправить запрос PUT /surveys/{generatedLink}/answers/{id} , 
где id - это id ответа в опросе. 
Если пользователь не авторизован, то сервер сообщит об ошибке доступа.

##**Закрытие опроса**

Закрыть опрос можно отправкой запроса PUT /surveys/{generatedLink}/close
Сервер вернет данные опроса.
Если пользователь не ялвяется создателем опроса, то сервер вернет ошибку 403 Forbidden.
Если пользователь не авторизован, то сервер сообщит об ошибке доступа.